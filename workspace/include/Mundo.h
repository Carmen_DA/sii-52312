// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DatosMemCompartida.h"
#include "Esfera.h"
//#include "Raqueta.h"
#include "Disparo.h"
#include "ListaDisparos.h"


class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	
	DatosMemCompartida *dat_p;
	DatosMemCompartida dat;
	
	//ListaDisparos misilesJ1;
	//ListaDisparos misilesJ2;
	
	//int misiles1=5;
	//int misiles2=5;
	
	char buf1[55]="Jugador 1 marca 1 punto, lleva un total de 0 puntos."; // 43
	char buf2[55]="Jugador 2 marca 1 punto, lleva un total de 0 puntos."; // 43
	
	char buf_r[50]="El juego se ha reiniciado.";
	
	char buf_g2[50]="El jugador 2 ha GANADO.";
	char buf_g1[50]="El jugador 1 ha GANADO.";
	
	int puntos1;
	int puntos2;
	
	int fd; // Tubería
	int fd_mem;
	char *org;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
