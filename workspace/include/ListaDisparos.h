// ListaDisparos.h: interface for the ListaDisparos class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LISTADISPAROS_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_)
#define AFX_LISTADISPAROS_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define MAX_DISPAROS 5

#include "Disparo.h"
//#include "Caja.h"

class ListaDisparos  
{
public:
	ListaDisparos();
	virtual ~ListaDisparos();
	
	bool agregar(Disparo* d);
	void destruirContenido();
	void Mueve(float t);
	void Dibuja();

//	void colision(Pared p);
//	void colision(Caja c);
	
private:
	Disparo * lista[MAX_DISPAROS];
	int numero;
};

#endif // !defined(AFX_LISTADISPAROS_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_)
