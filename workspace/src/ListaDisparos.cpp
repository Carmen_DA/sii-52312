// ListaDisparos.cpp: implementation of the ListaDisparos class.
// CARMEN D�EZ ALMOGUERA, 52312
//////////////////////////////////////////////////////////////////////

#include "ListaDisparos.h"
//#include "Interaccion.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

ListaDisparos::ListaDisparos()
{

}

ListaDisparos::~ListaDisparos()
{

}

void ListaDisparos::Dibuja()
{
	for(int i=0;i<numero;i++)
		lista[i]->Dibuja();
}

void ListaDisparos::Mueve(float t)
{
	for(int i=0;i<numero;i++)
		lista[i]->Mueve(t);
}

void ListaDisparos::destruirContenido()
{
	for(int i=0;i<numero;i++)
		delete lista[i];

	numero=0;
}

bool ListaDisparos::agregar(Disparo *d)
{
	for(int i=0;i<numero;i++) // Para evitar que se a�ada un disparo ya existente
		if(lista[i]==d)
			return false;

	if(numero<MAX_DISPAROS)
	   lista[numero++]=d;
	else 
		return false;
	return true;
}

/* void ListaDisparos::colision(Pared p)
{
	for(int i=0;i<numero;i++)
	{
		if(Interaccion::colision(*(lista[i]),p))
		{
			lista[i]->setVel(0,0);
		}
	}
}

void ListaDisparos::colision(Caja c)
{
	for(int i=0;i<numero;i++)
	{
		if(Interaccion::colision(*(lista[i]),c))
		{
			lista[i]->setVel(0,0);
			
		}
	}
}*/
