// Disparo.cpp: implementation of the Disparo class.
// CARMEN DÍEZ ALMOGUERA, 52312
//////////////////////////////////////////////////////////////////////

#include "Disparo.h"
#include "glut.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Disparo::Disparo(float v)
{
	radio=0.15f;
	velocidad.x=v;
	velocidad.y=0;
}

Disparo::~Disparo()
{

}

void Disparo::Dibuja()
{
	glColor3ub(108,238,32);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Disparo::Mueve(float t)
{
        centro=centro+velocidad*t;
}
	
