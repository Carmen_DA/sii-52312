// BOT
// CARMEN DÍEZ ALMOGUERA, 52312
//////////////////////////////////////////////////////////////////

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <iostream>

#include "DatosMemCompartida.h"

int main(void)
{
	DatosMemCompartida *dat_p;
	char *org;
	int fd_mem;
	
	fd_mem=open("DatosComp.txt", O_RDWR);
	
	if(fd_mem<0)
	{
		perror("open");
		return 1;
	}
	
	org=(char*) mmap(NULL, sizeof(*(dat_p)), PROT_WRITE|PROT_READ, MAP_SHARED, fd_mem, 0);
	
	close(fd_mem);
	
	dat_p=(DatosMemCompartida *) org;
	
	while(1) // bucle infinito, control del J1
	{
		float pos_J1_x;
		float pos_J1_y;
		
		pos_J1_x=(dat_p->raqueta1.x1);
		pos_J1_y=(dat_p->raqueta1.y1+dat_p->raqueta1.y2)/2;
		
		if (pos_J1_y < dat_p->esfera.centro.y)
			dat_p->accion=1;
			
		else if (pos_J1_y > dat_p->esfera.centro.y)
			dat_p->accion=-1;
			
		else 
			dat_p->accion=0;
		
		usleep(25000);		
	}
	
	munmap(org, sizeof(*(dat_p)));
	return 0;
} 



