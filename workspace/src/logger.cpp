// LOGGER
// CARMEN DÍEZ ALMOGUERA, 52312
//////////////////////////////////////////////////////////////////

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

int main(int argc, char **argv)
{
	char buf[128];

	if(mkfifo("/tmp/Tuba", 0777)!=0)
		printf("Error al crear FIFO. \n");

	int fd = open("/tmp/Tuba", O_RDONLY);

	if (fd<0)
	{
		perror("open");
		return 1;
	}

	while (read(fd, buf, 40))
		printf("%s \n", buf);

	close(fd);

	int r=unlink("/tmp/Tuba");

	if(r<0)
	{
		perror("unlink");
		return 1;
	}

	return 0;
} 


