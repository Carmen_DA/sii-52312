1.- En las primeras líneas de código de cada .cpp he escrito CARMEN DÍEZ ALMOGUERA, 52312.

2.- Se ha implementado el hecho de que cada vez que se dé cierto número de colisiones entre las raquetas y la esfera (3 en este caso), 
    se reduzca al 75% el tamaño de la esfera. Cuando la esfera toque una de las paredes que otorgan puntos a los jugadores, el contador 
    de colisiones se pondrá a cero y el tamaño de la esfera volverá a su valor inicial (0.5 en este caso).

3.- Se crea el documento logger.cpp en el que se crea el FIFO. Se incluye en Mundo.cpp código relacionado con la escritura del seguimiento 
    de los puntos de los jugadores según va avanzando el juego y se hace que el juego se termine cuando uno de los jugadores alcance los 3 puntos.
    Se crea el documento bot.cpp que se encargará de mover la raqueta del Jugador 1 y así, poder jugar "contra la máquina". 
